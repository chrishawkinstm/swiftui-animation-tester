//
//  Trace.swift
//  AnimationTester
//
//  Created by chawkins on 25/04/21.
//

import SwiftUI

struct Trace: Shape {
    var values: [(CGFloat, CGFloat)]
    
    func path(in rect: CGRect) -> Path {
        guard let f = values.first, let l = values.last else { return Path() }
        let xOffset = f.0
        let xMultiplier = l.0 - f.0
        return Path { p in
            p.move(to: CGPoint(x: rect.minX, y: rect.maxY))
            for value in values {
                let point = CGPoint(x: rect.minX + ((value.0 - xOffset) / xMultiplier) * rect.width, y: rect.maxY - CGFloat(value.1) * rect.height)
                p.addLine(to: point)
            }
        }
    }
    
}
