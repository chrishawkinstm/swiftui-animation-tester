//
//  PositionLoggingGeometryEffect.swift
//  AnimationTester
//
//  Created by chawkins on 25/04/21.
//

import SwiftUI

struct PositionLoggingGeometryEffect: GeometryEffect {
    var onUpdate: (AnimatableData) -> Void
    
    var animatableData: CGFloat = .zero {
        didSet {
            onUpdate(animatableData)
        }
    }
    
    func effectValue(size: CGSize) -> ProjectionTransform {
        ProjectionTransform(.identity)
    }
}
