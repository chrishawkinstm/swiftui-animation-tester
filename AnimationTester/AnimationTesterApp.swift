//
//  AnimationTesterApp.swift
//  AnimationTester
//
//  Created by chawkins on 25/04/21.
//

import SwiftUI

@main
struct AnimationTesterApp: App {
    var body: some Scene {
        WindowGroup {
            AnimationTesterView()
        }
    }
}
