//
//  AnimationTraceViewModel.swift
//  AnimationTester
//
//  Created by chawkins on 25/04/21.
//

import SwiftUI

class AnimationTraceViewModel: ObservableObject {
    @Published
    var values: [(time: CGFloat, value: CGFloat)] = []
    
    private var initialDate: Date? = nil
    
    func record(value: CGFloat) {
        
        if initialDate == nil {
            initialDate = Date()
        }
        let elapsed = CGFloat(Date().timeIntervalSince(initialDate!))
        DispatchQueue.main.async {
            guard value != self.values.last?.value else { return }
            self.values.append((elapsed, value))
        }
    }
    
    func reset() {
        values = []
        initialDate = nil
    }
}
