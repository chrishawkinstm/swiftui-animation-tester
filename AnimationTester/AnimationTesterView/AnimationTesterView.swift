//
//  AnimationTesterView.swift
//  AnimationTesterView
//
//  Created by chawkins on 25/04/21.
//
import SwiftUI

import Combine

struct AnimationTesterView: View {
    
    @ObservedObject
    var viewModel: AnimationTesterViewModel = AnimationTesterViewModel()
    
    @State
    var animationType: AnimationType = .spring
    
    @ObservedObject
    var animationTraceViewModel = AnimationTraceViewModel()
    
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        VSplitView {
            HSplitView {
                Form {
                    VStack(alignment: .leading) {
                        GroupBox(label: Text("AnimationType").bold()) {
                            HStack {
                                Picker("", selection: $viewModel.currentAnimationType) {
                                    ForEach(viewModel.animationTypes) { animationType in
                                        Text(animationType.rawValue).tag(animationType)
                                    }
                                }.pickerStyle(RadioGroupPickerStyle())
                                Spacer()
                            }.padding()
                        }
                        Spacer().frame(height: 16)

                        GroupBox(label: Text("Slow Motion").bold()) {
                            HStack {
                                Picker("", selection: $viewModel.slowMoMode) {
                                    ForEach(SlowMoMode.allCases) { slowMoMode in
                                        Text(slowMoMode.rawValue).tag(slowMoMode)
                                    }
                                }.pickerStyle(RadioGroupPickerStyle())
                                Spacer()
                            }.padding()
                        }
                        Spacer().frame(height: 16)
                        
                        GroupBox(label: Text("Properties").bold()) {
                            VStack {
                                ForEach(viewModel.currentAnimation.properties) { property in
                                    let propertyBinding = viewModel.bindingFor(propertyType: property.type, on: viewModel.currentAnimationType)
                                    AnimationPropertySlider(property: propertyBinding)
                                }
                            }.padding()
                        }
                        Spacer().frame(height: 16)
                        
                        GroupBox(label: Text("Curve").bold()) {
                            VStack {
                                Trace(values: animationTraceViewModel.values.map {
                                    (CGFloat($0), $1)
                                })
                                    .stroke(Color.red, style: .init(lineWidth: 2))
                                    .frame(idealHeight: 200)
                            }
                            .padding()
                            .padding(.vertical, 24)
                        }
                        Spacer(minLength: 16)
                        
                        HStack {
                            Spacer()
                            Button("Animate") {
                                animationTraceViewModel.reset()
                                viewModel.selectAnimate()
                            }
                        }
                    }
                }
                .padding()
                .frame(width: 300)
                
                GeometryReader { geometry in
                    VStack(spacing:0) {
                        Divider().edgesIgnoringSafeArea(.horizontal)
                        let cardHeight: CGFloat = 100
                        let padding: CGFloat = 16
                        
                        Card()
                            .offset(x: 0, y: viewModel.cardPinnedToTop ? geometry.size.height - cardHeight - (padding) : padding)
                            .modifier(PositionLoggingGeometryEffect(onUpdate: {
                                print($0)
                                let modifiedValue: CGFloat = viewModel.cardPinnedToTop ? $0 : 1-$0
                                self.animationTraceViewModel.record(value: modifiedValue)
                            }, animatableData: viewModel.cardPinnedToTop ? 1 : 0))
                            .frame(height: cardHeight)

                        Spacer()
                    }
                    .padding(.horizontal)
                    .background(colorScheme == .dark ? Color.black : Color.white)
                    .frame(minWidth: 100, minHeight: 400)
                }
            }
            VStack {
                TextField("Snippet", text: .constant(viewModel.currentSnippet))
            }
        }
    }
}

struct AnimationPropertySlider: View {
    @Binding
    var property: AnimationPropertyModel
    
    var body: some View {
        let valueBinding = $property.value
        Slider(value: valueBinding, label: {
            VStack(alignment: .leading) {
                Text(property.name)//.bold()
                //                Text("\(property.value)")
            }//.frame(width: 100)
        })
    }
}

struct Card: View {
    
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        VStack(alignment: .leading) {
            Text("Some animating view").font(.headline)
            Text("This is an example of the kind of view you may want to animate.")
            Spacer()
        }
        .padding()
        .background(colorScheme == .dark ? Color(white: 0.1) : Color.white)
        .cornerRadius(8.0)
        .shadow(radius: 10)
    }
}

struct AnimationTesterView_Previews: PreviewProvider {
    static var previews: some View {
        AnimationTesterView()
            .previewLayout(.sizeThatFits)
    }
}
