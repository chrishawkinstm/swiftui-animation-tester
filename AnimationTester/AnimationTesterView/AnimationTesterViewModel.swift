//
//  AnimationTesterViewModel.swift
//  AnimationTester
//
//  Created by chawkins on 25/04/21.
//

import Foundation
import Combine
import SwiftUI

class AnimationTesterViewModel: ObservableObject {
    
    @Published
    var properties: [AnimationType: [AnimationPropertyModel]] = [:]
    
    @Published
    var animationTypes: [AnimationType] = AnimationType.allCases
    
    @Published
    var currentAnimationType: AnimationType
    
    @Published
    var cardPinnedToTop: Bool = true
    
    @Published
    var slowMoMode: SlowMoMode = .normal
    
    var currentAnimation: AnimationModel {
        AnimationModel (
            type: currentAnimationType,
            properties: properties[currentAnimationType] ?? []
        )
    }
    
    
    init() {
        var accumulator: [AnimationType: [AnimationPropertyModel]] = [:]
        for animationType in AnimationType.allCases {
            let properties = animationType.relevantPropertyTypes
                .map { AnimationPropertyModel(type: $0, value: 0.5) }
            accumulator[animationType] = properties
        }
        self.properties = accumulator
        self.currentAnimationType = .linear
    }
    
    func bindingFor(propertyType: AnimationPropertyType, on animationType: AnimationType) -> Binding<AnimationPropertyModel> {
        .init { () -> AnimationPropertyModel in
            let propertyIndex = animationType.relevantPropertyTypes.firstIndex(of: propertyType)!
            return self.properties[animationType]![propertyIndex]
        } set: { newValue in
            let propertyIndex = animationType.relevantPropertyTypes.firstIndex(of: propertyType)!
            self.properties[animationType]![propertyIndex] = newValue
        }
    }
    
    func selectAnimate() {
        withAnimation(createAnimation(for: currentAnimation).speed(slowMoMode.multiplier)) {
            cardPinnedToTop.toggle()
        }
    }
    
    func createAnimation(for animation: AnimationModel) -> Animation {
        switch animation.type {
        case .theDefault:
            return .default
        case .linear:
            return .linear(
                duration: animation.value(for: .duration)
            )
        case .interploatingSpring:
            return .interpolatingSpring(
                mass: animation.value(for: .mass),
                stiffness: animation.value(for: .stiffness),
                damping: animation.value(for: .damping),
                initialVelocity: animation.value(for: .initialVelocity)
            )
        case .easeInOut:
            return .easeInOut(
                duration: animation.value(for: .duration)
            )
        case .easeIn:
            return .easeIn(
                duration: animation.value(for: .duration)
            )
        case .easeOut:
            return .easeOut(
                duration: animation.value(for: .duration)
            )
        case .interactiveSpring:
            return .interactiveSpring(
                response: animation.value(for: .response),
                dampingFraction: animation.value(for: .dampingFraction),
                blendDuration: animation.value(for: .blendDuration)
            )
        case .spring:
            return .spring(
                response: animation.value(for: .response),
                dampingFraction: animation.value(for: .damping),
                blendDuration: animation.value(for: .blendDuration)
            )
        case .customCurve:
            return .timingCurve(
                animation.value(for: .c0x),
                animation.value(for: .c0y),
                animation.value(for: .c1x),
                animation.value(for: .c1y)
            )
        }
    }
    
    var currentSnippet: String {
        let animation = currentAnimation
        switch animation.type {
        case .theDefault:
            return ".default"
        case .linear:
            return ".linear(duration: \(animation.value(for: .duration)))"
        case .interploatingSpring:
            return ".interpolatingSpring(mass: \(animation.value(for: .mass)), stiffness: \(animation.value(for: .stiffness)), damping: \(animation.value(for: .damping)), initialVelocity: \(animation.value(for: .initialVelocity)))"
        case .easeInOut:
            return ".easeInOut(duration: \(animation.value(for: .duration)))"
        case .easeIn:
            return ".easeIn(duration: \(animation.value(for: .duration)))"
        case .easeOut:
            return ".easeOut(duration: \(animation.value(for: .duration)))"
        case .interactiveSpring:
            return ".interactiveSpring(response: \(animation.value(for: .response)), dampingFraction: \(animation.value(for: .dampingFraction)), blendDuration: \(animation.value(for: .blendDuration)))"
        case .spring:
            return ".spring(response: \(animation.value(for: .response)), dampingFraction: \(animation.value(for: .damping)), blendDuration: \(animation.value(for: .blendDuration)))"
        case .customCurve:
            return ".timingCurve(\(animation.value(for: .c0x)), \(animation.value(for: .c0y)), \(animation.value(for: .c1x)), \(animation.value(for: .c1y)))"
        }
    }
}

extension AnimationModel {
    func value(for propertyType: AnimationPropertyType) -> Double {
        properties.first(where: { $0.type == propertyType })!.value
    }
}

struct AnimationPropertyModel: Identifiable, Hashable {
    var type: AnimationPropertyType
    
    var name: String {
        return type.rawValue
    }
    
    var value: Double
    
    var id: String {
        return type.rawValue
    }
}

struct AnimationModel: Identifiable, Hashable {
    var type: AnimationType
    var properties: [AnimationPropertyModel]
    
    var name: String {
        return type.rawValue
    }
    
    var id: String {
        return type.rawValue
    }
        
}

enum SlowMoMode: String, Hashable, Identifiable, CaseIterable {
    case normal = "Normal"
    case slow = "Slow"
    case superSlow = "Super slow"
    case crazySlow = "Crazy slow"
    
    var multiplier: Double {
        switch self {
        case .normal:       return 1
        case .slow:         return 0.25
        case .superSlow:    return 0.1
        case .crazySlow:    return 0.025
        }
    }
    
    var id: String {
        rawValue
    }
}
