//
//  AnimationPropertyType.swift
//  AnimationTester
//
//  Created by chawkins on 25/04/21.
//

import Foundation

enum AnimationPropertyType: String {
    case duration = "Duration"
    case stiffness = "Stiffness"
    case damping = "Damping"
    case response = "response"
    case dampingFraction = "Damping fraction"
    case blendDuration = "Blend duration"
    case initialVelocity = "Initial Velocity"
    case mass = "Mass"
    case c0x = "c0x"
    case c0y = "c0y"
    case c1x = "c1x"
    case c1y = "c1y"
    
    var range: ClosedRange<Double> {
        return 0...5
    }
}
