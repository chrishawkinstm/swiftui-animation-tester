//
//  AnimationType.swift
//  AnimationTester
//
//  Created by chawkins on 25/04/21.
//

import Foundation

enum AnimationType: String, Hashable, CaseIterable, Identifiable {
    case theDefault = "Default"
    case linear = "Linear"
    case interploatingSpring = "Interpolating Spring"
    case easeInOut = "Ease In Out"
    case easeIn = "Ease In"
    case easeOut = "Ease Out"
    case interactiveSpring = "Interactive Spring"
    case spring = "Spring"
    case customCurve = "Custom curve"
    
    var relevantPropertyTypes: [AnimationPropertyType] {
        switch self {
        case .theDefault:
            return []
        case .linear:
            return [.duration]
        case .interploatingSpring:
            return [.stiffness, .damping, .initialVelocity, .mass]
        case .easeInOut:
            return [.duration]
        case .easeIn:
            return [.duration]
        case .easeOut:
            return [.duration]
        case .interactiveSpring:
            return [.response, .dampingFraction, .blendDuration]
        case .spring:
            return [.response, .damping, .blendDuration]
        case .customCurve:
            return [.c0x, .c0y, .c1x, .c1y]
        }
    }
    
    var id: String {
        rawValue
    }
    
    var name: String {
        rawValue
    }
}
